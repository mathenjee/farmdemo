package com.ndtt.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ndtt.domain.Coordinate;
import com.ndtt.domain.dto.ActionDto;

@Service
public interface ActionService {

	ActionDto create(ActionDto actionDto);

	ActionDto update(ActionDto actionDto);

	ActionDto findById(String id);

	List<ActionDto> findByActor(String actor);

	List<ActionDto> findByActionDtoType(String actionType);

	List<ActionDto> findByCoordinate(Coordinate coordinate);

	List<ActionDto> findByDateIsGreaterThanEqualAndDateIsLessThanEqual(Date from, Date to);

	List<ActionDto> findByDate(Date date);

	boolean deleteById(String id);

	boolean updateImageLinks(String id, List<String> imageLinks);

}
