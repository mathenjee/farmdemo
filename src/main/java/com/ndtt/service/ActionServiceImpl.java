package com.ndtt.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndtt.domain.Coordinate;
import com.ndtt.domain.dto.ActionDto;
import com.ndtt.repository.ActionRepository;

import lombok.ToString;

@Service
@ToString
public class ActionServiceImpl implements ActionService {

	@Autowired
	private ActionRepository actionRepository;

	@Override
	public ActionDto create(ActionDto actionDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActionDto update(ActionDto actionDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActionDto findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActionDto> findByActor(String actor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActionDto> findByActionDtoType(String actionType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActionDto> findByCoordinate(Coordinate coordinate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActionDto> findByDateIsGreaterThanEqualAndDateIsLessThanEqual(Date from, Date to) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActionDto> findByDate(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteById(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateImageLinks(String id, List<String> imageLinks) {
		// TODO Auto-generated method stub
		return false;
	}

}
