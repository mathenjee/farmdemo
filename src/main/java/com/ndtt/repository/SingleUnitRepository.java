package com.ndtt.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ndtt.domain.Action;
import com.ndtt.domain.SingleUnit;

public interface SingleUnitRepository extends MongoRepository<SingleUnit, String> {

	SingleUnit findOneByName(String name);

	List<SingleUnit> findByAddress(String address);

	List<SingleUnit> findByLevel(byte level);

	List<SingleUnit> findByOwnersContains(List<String> owners);

	List<SingleUnit> findByActionsContains(List<Action> actions);

}
