package com.ndtt.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ndtt.domain.Action;
import com.ndtt.domain.ComplexUnit;
import com.ndtt.domain.Unit;

public interface ComplexUnitRepository extends MongoRepository<ComplexUnit, String> {

	ComplexUnit findOneByName(String name);

	List<ComplexUnit> findByAddress(String address);

	List<ComplexUnit> findByLevel(byte level);

	List<ComplexUnit> findByOwnersContains(List<String> owners);

	List<ComplexUnit> findByActionsContains(List<Action> actions);

	ComplexUnit findByUnitsContains(List<Unit> units);

}
