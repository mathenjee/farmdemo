package com.ndtt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ndtt.domain.Action;
import com.ndtt.domain.Coordinate;

public interface ActionRepository extends MongoRepository<Action, String> {

	List<Action> findByActor(String Actor);

	List<Action> findByActionType(String actionType);

	List<Action> findByCoordinate(Coordinate coordinate);

	List<Action> findByDateIsGreaterThanEqualAndDateIsLessThanEqual(Date from, Date to);

	List<Action> findByDate(Date date);

}
