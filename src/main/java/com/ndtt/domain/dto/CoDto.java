package com.ndtt.domain.dto;

import org.hibernate.validator.constraints.NotBlank;

public class CoDto {

	@NotBlank
	private String longitude;
	@NotBlank
	private String latitude;
}
