package com.ndtt.domain.dto;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import com.ndtt.domain.Coordinate;

import lombok.Data;

@Data
public abstract class UnitDto {
	protected String id;
	@NotBlank
	protected String name;
	@NotBlank
	protected String address;
	@Range(max = 3, min = 1)
	protected byte level;
	protected Coordinate coordinate;
}
