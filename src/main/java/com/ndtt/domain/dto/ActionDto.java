package com.ndtt.domain.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.ndtt.domain.Coordinate;

import lombok.Data;

@Data
public class ActionDto {

	private String id;
	private Date date;
	@NotBlank
	private String actor;
	@NotBlank
	private String actionType;
	private String note;
	@NotNull
	private Coordinate coordinate;
}
