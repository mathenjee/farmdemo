package com.ndtt.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Action {
	@Id
	private String id;
	private Date date;
	@Indexed
	private String actor;
	@Indexed
	private String actionType;
	private List<String> imageLinks;
	private String note;
	private Coordinate coordinate;
}
