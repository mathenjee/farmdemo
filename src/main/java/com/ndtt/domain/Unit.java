package com.ndtt.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public abstract class Unit {
	@Id
	protected String id;
	protected String name;
	protected String address;
	protected byte level;
	protected List<String> owners;
	protected Coordinate coordinate;
	@DBRef(lazy = true)
	private List<Action> actions;

}
