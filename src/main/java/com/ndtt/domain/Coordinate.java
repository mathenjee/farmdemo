package com.ndtt.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Coordinate {
	private String longitude;
	private String latitude;
}
