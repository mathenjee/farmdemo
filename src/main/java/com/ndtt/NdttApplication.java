package com.ndtt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
public class NdttApplication {

	public static void main(String[] args) {
		SpringApplication.run(NdttApplication.class, args);

	}
}
