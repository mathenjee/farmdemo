package com.ndtt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ndtt.domain.Action;
import com.ndtt.domain.Coordinate;
import com.ndtt.domain.SingleUnit;
import com.ndtt.repository.ActionRepository;
import com.ndtt.repository.SingleUnitRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Qualifier
public class NdttApplicationTests {
	@Autowired
	private SingleUnitRepository singleUnitRepository;
	@Autowired
	private ActionRepository actionRepository;

	@Test
	public void contextLoads() {
		Action action = new Action();
		action.setActionType("clean");
		action.setActor("Jane");
		action.setDate(new Date());
		action.setCoordinate(new Coordinate());
		action = actionRepository.save(action);
		List<Action> actions = new ArrayList<>();
		actions.add(action);
		SingleUnit singleUnit = new SingleUnit();
		singleUnit.setActions(actions);
		singleUnit.setAddress("Street 12");
		singleUnit.setName("Fish");
		List<String> owners = new ArrayList<>();
		owners.add("Jane");
		singleUnit.setOwners(owners);
		singleUnit.setLevel((byte) 0);
		singleUnit = singleUnitRepository.save(singleUnit);
		Log log = LogFactory.getLog(getClass());
		log.info(action);
		log.info(singleUnit);
	}
}